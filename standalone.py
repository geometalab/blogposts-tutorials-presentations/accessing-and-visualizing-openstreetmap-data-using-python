import plotly.express as px
import geopandas as gpd
import matplotlib.pyplot as plt

from collections import namedtuple
from osmxtract import overpass
from osm2geojson import json2geojson


OVERPASS_ENDPOINT = "http://overpass.osm.ch/api/interpreter"


# BBox with components ordered as expected by overpass.ql_query
Bbox = namedtuple("Bbox", ["south", "west", "north", "east"])

Tag = namedtuple("Tag", ["key", "values"])


def main():
    tag = Tag(key="sport", values=["table_tennis"])
    bbox_zurich = Bbox(west=8.471668, south=47.348834, east=8.600454, north=47.434379)
    table_tennis_gdf = load_osm_from_overpass(bbox_zurich, tag)
    gdf = table_tennis_gdf.to_crs(epsg=3857)
    gdf.geometry = gdf.geometry.centroid
    gdf = gdf.to_crs(epsg=4236)
    fig = plot_gdf_on_map(gdf, title="Zurich Table Tennis")
    # if no window pops up, uncomment the following line
    # and open the standalone_map.html in your browser
    # fig.write_html("standalone_map.html")
    fig.show()


def load_osm_from_overpass(bbox, tag, crs="epsg:4326") -> gpd.GeoDataFrame:
    geojson = load_osm_from_overpass_geojson(bbox, tag, crs)
    return gpd.GeoDataFrame.from_features(geojson, crs=crs)
    # TODO: Issue is that in OSM an object can be point or linestring or area
    # Solution: See https://gis.stackexchange.com/questions/302430/polygon-to-point-in-geopandas


def load_osm_from_overpass_geojson(bbox, tag, crs="epsg:4326"):
    values = None if "*" in tag.values else tag.values
    query = overpass.ql_query(bbox, tag=tag.key, values=values)
    response = overpass.request(query, endpoint=OVERPASS_ENDPOINT)
    return json2geojson(response)


def plot_gdf_on_map(gdf, color="k", title=""):
    fig = px.scatter_mapbox(
        gdf,
        lat=gdf.geometry.y,
        lon=gdf.geometry.x,
        color_discrete_sequence=["fuchsia"],
        height=500,
        zoom=11,
    )
    fig.update_layout(title=title, mapbox_style="open-street-map")
    fig.update_layout(margin={"r": 0, "l": 0, "b": 0})
    return fig


if __name__ == "__main__":
    main()