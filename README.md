# Accessing and Visualizing OpenStreetMap Data using Python

This repository contains the source code files for
"Accessing and Visualizing OpenStreetMap Data using Python".

Launch on Binder:

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/geometalab%2Fblogposts-tutorials-presentations%2Faccessing-and-visualizing-openstreetmap-data-using-python/master?filepath=index.ipynb)

## Dependencies

For the libraries used in the Notebook,
see [`environment.yml` in the `binder` directory](binder/environment.yml).
If you aren't using Binder,
you can install these libraries using `conda`
or you can use the `Dockerfile` in this repo
to build a container image containing everything.

## docker usage

To run locally using docker, you need docker-compose installed.

Running is quite simple:

```bash
docker-compose up --build
```

Open Browser on [http://localhost:8888](http://localhost:8888).
